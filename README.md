Module to manage currency
=========================
Module to manage currency list and update cources

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Edit your composer.json

```
    "repositories": [
...
        {
            "type": "git",
            "url": "https://xtetis@bitbucket.org/xtetis/yii2-currency.git"
        }
...
    ]
```

Either run

```
php composer.phar require --prefer-dist xtetis/yii2-currency "dev-master"
```

or add

```
"xtetis/yii2-currency":"dev-master"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
    'modules' => [
...
        'currency' => [
            'class' => 'xtetis\currency\Module',
            'as access' => [
                'class' => yii2mod\rbac\filters\AccessControl::class,
            ],
        ],
...
    ],
```


Migrations
-----
```
    ./yii migrate/up --migrationPath=@vendor/xtetis/yii2-currency/src/migrations
```



# Модуль для управления данными о валютах
Составляет из себя модуль для работы со валютами и их курсами
В составе:
- Модели для достаупа к нужным таблицам
- Модуль для управления данными в базе
- Миграция для создания необходимых таблиц в базе