<?php

namespace xtetis\currency\models;

use Yii;

/**
 * This is the model class for table "{{%currency}}".
 *
 * @property int $id Первичный ключ
 * @property string $name Название валюты
 * @property string $code Код Должна соответствовать стандарту ISO code
 * @property string $shortname Символ слева
 * @property double $value Значение: Установите на 1.00000 если это валюта по умолчанию.
 */
class Currency extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%currency}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['value'], 'number'],
            [['name', 'code', 'shortname'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Первичный ключ',
            'name' => 'Название валюты',
            'code' => 'Код (ISO code)',
            'shortname' => 'Символ слева',
            'value' => 'Значение (1.00000 если валюта по-умолчанию)',
        ];
    }
}
