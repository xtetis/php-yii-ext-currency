<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%currency}}`.
 */
class m190404_185832_create_currency_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%currency}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(200)->append('CHARACTER SET utf8 COLLATE utf8_general_ci'),
            'code' => $this->string(200)->append('CHARACTER SET utf8 COLLATE utf8_general_ci'),
            'shortname' => $this->string(200)->append('CHARACTER SET utf8 COLLATE utf8_general_ci'),
            'value' => $this->float()->defaultValue(1),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        $this->addCommentOnColumn('{{%currency}}','id', 'Первичный ключ');
        $this->addCommentOnColumn('{{%currency}}','name', 'Название валюты');
        $this->addCommentOnColumn('{{%currency}}','code', 'Код Должна соответствовать стандарту ISO code');
        $this->addCommentOnColumn('{{%currency}}','shortname', 'Символ слева');
        $this->addCommentOnColumn('{{%currency}}','value', 'Значение: Установите на 1.00000 если это валюта по умолчанию.');
        

        $this->addCommentOnTable('{{%currency}}','Список валют');


        $currency = array(
            array('id' => '1','name' => 'Гривны','code' => 'UAH','shortname' => 'грн.','value' => '1')
          );


          foreach ($currency as $v) {
            $this->insert('currency', $v);
        }
          
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%currency}}');
    }
}
